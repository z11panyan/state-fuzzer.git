# stateFuzzer

#### Description
stateFuzzer is a tool for learning-based greybox fuzzing, which consists of automata leanring and state-aware fuzzing. It can test the server and client of the protocol respectively.

#### Requirements
graphviz

#### Installation

1.  Build a self-contained jar file using the following command:
```
mvn package shade:shade
```
2.  Modify the target implementation code
```
## openssl.c
#include <sanitizer/coverage_interface.h>
static int kNumPCs = 1 << 21;
size_t numPCs;
uint8_t __sancov_trace_pc_guard_8bit_counters[1 << 21];
uintptr_t __sancov_trace_pc_pcs[1 << 21];
char * bitmap;
#ifdef __cplusplus
extern "C" {
#endif
	void __sanitizer_cov_trace_pc_guard_init(uint32_t *start,
		                                            uint32_t *stop) {
	  numPCs = 0;
	  if (start == stop || *start) return;  // Initialize only once.
	  printf("INIT: %p %p\n", start, stop);
	  for (uint32_t *x = start; x < stop; x++)
	    *x = ++numPCs;  // Guards should start from 1.
		printf("Total edges : %d\n", numPCs);
	  bitmap = calloc(numPCs+1, sizeof(char));
	}
	 
	void __sanitizer_cov_trace_pc_guard(uint32_t *guard) {
	  if (!*guard) return;  // Duplicate the guard check.
	  intptr_t PC = __builtin_return_address(0);
	  uint32_t Idx = *guard;
	  __sancov_trace_pc_pcs[Idx] = PC;
	  __sancov_trace_pc_guard_8bit_counters[Idx]++;
	}

	size_t GetTotalPCCoverage() {
	  size_t Res = 0;
	  for (size_t i = 0, N = numPCs; i < N; i++)
	    if (__sancov_trace_pc_pcs[i])
	      Res++;
	  return Res;
	}

	void GetBitmap() {
	  for (size_t i = 0, N = numPCs; i < N; i++)
	  {
	    if (__sancov_trace_pc_pcs[i])
		bitmap[i] = '1';
	    else
		bitmap[i] = '0';
	  }
          bitmap[numPCs]='\0';
	  printf("bitmap: %s\n",bitmap);
	}
   
	void ResetCoverage() {  
	  for (size_t i = 0, N = numPCs; i < N; i++)
	  {
	      __sancov_trace_pc_pcs[i] = 0;
	  }
 	  free(bitmap);
	}
#ifdef __cplusplus
}
#endif
```

```
## s_server.c (sv_body、www_body)
if (ret >= 0)
		BIO_printf(bio_s_out,"ACCEPT WWW\n");
	
	BIO_printf(bio_s_out,"Coverage: %d \n",GetTotalPCCoverage());
	(void)BIO_flush(bio_s_out);
	if (buf != NULL) OPENSSL_free(buf);
```

```
## s_client.c (s_client_main) 
    BIO_printf(bio_c_out,"bitmap: %s \n",GetBitmap());
    (void)BIO_flush(bio_c_out);    
```

3.  Compile the target 
```
CC="clang" CFLAGS="-O2 -fno-omit-frame-pointer -g -fsanitize=address -fsanitize-coverage=trace-pc-guard,trace-cmp,trace-gep,trace-div -DFUZZER_DISABLE_SIGNCHECK" ./config no-shared -fPIC --prefix=/home/xxxx/test/openssl-3.0.0/build --openssldir=/home/xxxx/test/openssl-3.0.0/build/openssl
make && make install
```

#### Usage

1.  modify the configuration file "server.properties"

2.  excute the command 
```
cd ./state-fuzzer/examples/openssl
java -jar ../../target/stateFuzzer-0.0.1-SNAPSHOT.jar <configuration file> <time>
```
3. replay the recorded test cases to get the code coverage

We can run the command in the file "run" or run the command as following.
```
#java -cp ../../target/stateFuzzer-0.0.1-SNAPSHOT.jar nl.cypherpunk.statefuzzer.Replay subject port SeedDir RunCommand gcovrCommand target(server or client) env step starttime endtime
java -cp ../../target/stateFuzzer-0.0.1-SNAPSHOT.jar nl.cypherpunk.statefuzzer.Replay 
openssl 4433 
/home/username/model/statefuzzer/examples/openssl/libressl3.2.1-server-1/Seed 
"/home/username/test/libressl-gcov/apps/openssl/openssl s_server -key server.key -cert server.crt -CAfile cacert.pem -accept 4433 -HTTP" 
"gcovr -r /home/username/test/libressl-gcov -s | grep '[lb][a-z]*:' | xargs"  
/home/username/test/libgcov_preload.so
server 5 0 7200
```

The parameters are explained in the code

#### Notes

The Testcases.rar is the test cases that caused crashes or discrepancies on TLS, SMTP and RTSP.

#### Publications

StateFuzzer has been used in the following publications:


