#!/bin/bash

folder=$1   #fuzzer result folder
pno=$2      #port number
step=$3     #step to skip running gcovr and outputting data to covfile
            #e.g., step=5 means we run gcovr after every 5 test cases
covfile=$4  #path to coverage file
fmode=$5    #file mode -- structured or not
            #fmode = 0: the test case is a concatenated message sequence -- there is no message boundary
            #fmode = 1: the test case is a structured file keeping several request messages
starttime=$6
endtime=$7
#delete the existing coverage file
rm $covfile; touch $covfile

#clear gcov data
gcovr -r /home/pany/test/live555-cov -s -d > /dev/null 2>&1

#output the header of the coverage file which is in the CSV format
#Time: timestamp, l_per/b_per and l_abs/b_abs: line/branch coverage in percentage and absolutate number
echo "Time,l_per,l_abs,b_per,b_abs" >> $covfile

#files stored in replayable-* folders are structured
#in such a way that messages are separated
if [ $fmode -eq "1" ]; then
  testdir="Seed"
  replayer="/home/pany/profuzzbench/aflnet/aflnet-replay"
else
  testdir="Seed"
  replayer="/home/pany/profuzzbench/aflnet/afl-replay"
fi

#process other testcases
count=0
for f in $(echo $folder/$testdir/*); do 
  time=$(stat -c %Y $f)
  echo $f
  if [ "$count" == "0" ]; then delta=$time; fi
  time=$(expr $time - $delta + $starttime)  
  if [ "$time" -gt "$endtime" ]; then break; fi
  #terminate running server(s)
  pkill testOnDemandR
  
  $replayer $f RTSP $pno 1 > /dev/null 2>&1 &
  timeout -k 0 -s SIGUSR1 3s /home/pany/test/live555-cov/testProgs/testOnDemandRTSPServer $pno > /dev/null 2>&1

  wait
  count=$(expr $count + 1)
  rem=$(expr $count % $step)
  if [ "$rem" != "0" ]; then continue; fi
  cov_data=$(gcovr -r /home/pany/test/live555-cov -s | grep "[lb][a-z]*:")
  l_per=$(echo "$cov_data" | grep lines | cut -d" " -f2 | rev | cut -c2- | rev)
  l_abs=$(echo "$cov_data" | grep lines | cut -d" " -f3 | cut -c2-)
  b_per=$(echo "$cov_data" | grep branch | cut -d" " -f2 | rev | cut -c2- | rev)
  b_abs=$(echo "$cov_data" | grep branch | cut -d" " -f3 | cut -c2-)
  
  echo "$time,$l_per,$l_abs,$b_per,$b_abs" >> $covfile
done

#ouput cov data for the last testcase(s) if step > 1
if [[ $step -gt 1 ]]
then
  time=$(stat -c %Y $f)
  time=$(expr $time - $delta + $starttime)
  if [ "$time" -le "$endtime" ]
  then
	  cov_data=$(gcovr -r /home/pany/test/live555-cov -s | grep "[lb][a-z]*:")
	  l_per=$(echo "$cov_data" | grep lines | cut -d" " -f2 | rev | cut -c2- | rev)
	  l_abs=$(echo "$cov_data" | grep lines | cut -d" " -f3 | cut -c2-)
	  b_per=$(echo "$cov_data" | grep branch | cut -d" " -f2 | rev | cut -c2- | rev)
	  b_abs=$(echo "$cov_data" | grep branch | cut -d" " -f3 | cut -c2-)
	  
	  echo "$time,$l_per,$l_abs,$b_per,$b_abs" >> $covfile
  fi
fi
