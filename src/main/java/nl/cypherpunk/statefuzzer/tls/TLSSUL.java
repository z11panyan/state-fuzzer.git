/*
 *  Copyright (c) 2016 Joeri de Ruiter
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package nl.cypherpunk.statefuzzer.tls;

import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;
import nl.cypherpunk.statefuzzer.fuzzing.SUT;
import nl.cypherpunk.statefuzzer.rtsp.RTSPConfig;
import nl.cypherpunk.statefuzzer.rtsp.RTSPTestService;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Joeri de Ruiter (joeri@cs.ru.nl)
 */
public class TLSSUL implements SUT<String, String> {
	Alphabet<String> alphabet;
	public TLSTestService tls;
	
	public TLSSUL(TLSConfig config) throws Exception {
		alphabet = Alphabets.fromList(Arrays.asList(config.alphabet.split(" ")));
		
		tls = new TLSTestService();
		
		tls.setTarget(config.target);
		tls.setHost(config.host);
		tls.setPort(config.port);
		tls.setCommand(config.cmd);
		tls.setRestartTarget(config.restart);
		tls.setReceiveMessagesTimeout(config.timeout);
		tls.setKeystore(config.keystore_filename, config.keystore_password);
		tls.setConsoleOutput(config.console_output);
		tls.setRedirectFile(config.redirectFile);
		//tls.setCoverageCmd(config.coverage_cmd);
		tls.setOutputDir(config.output_dir);
		File outputDir = new File(tls.outputDir + File.separator +"Seed");
		outputDir.mkdirs();
		if(config.version.equals("tls10")) {
			tls.useTLS10();
		}
		else {
			tls.useTLS12();
		}
		
		tls.start();
	}

	public TLSSUL(TLSConfig config, int i) throws Exception {
		alphabet = Alphabets.fromList(Arrays.asList(config.alphabet.split(" ")));

		tls = new TLSTestService();

		tls.setTarget(config.target);
		tls.setHost(config.host);
		tls.setPort(config.portList.get(i));
		tls.setCommand(config.cmdList.get(i));
		tls.setRestartTarget(config.restart);
		tls.setReceiveMessagesTimeout(config.timeout);
		tls.setKeystore(config.keystore_filename, config.keystore_password);
		tls.setConsoleOutput(config.console_output);
		tls.setRedirectFile(config.redirectFile);
		//tls.setCoverageCmd(config.coverage_cmd);
		tls.setOutputDir(config.output_dir);
		File outputDir = new File(tls.outputDir + File.separator +"Seed");
		outputDir.mkdirs();
		File crashDir = new File(tls.outputDir + File.separator +"Crash");
		crashDir.mkdirs();
		if(config.version.equals("tls10")) {
			tls.useTLS10();
		}
		else {
			tls.useTLS12();
		}

		tls.start();
	}
	
	public Alphabet<String> getAlphabet() {
		return alphabet;
	}

	public void exit()
	{
		tls.close();
	}

	public boolean canFork() {
		return false;
	}
	//change by pany
	public void setInitScore(Double score){
		tls.initScore = score;
	}

	public int getCoverage() throws IOException {
		return tls.getCoverage();
	}


	public String SendFuzzingMessage(byte[] message) throws Exception
	{
		return tls.sendFuzzingMessage(message);
	}

	public byte[] getCurrent(){
		return tls.currentCase.get(tls.currentCase.size()-1);
	}

	@Override
	public String messageToSymbol(byte[] message){
		String[] tmp = message.toString().split(" ");
		return tmp[0];
	}

	@Override
	public String step(String symbol) {
		String result = null;
		try {
			result = tls.processSymbol(symbol);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void pre() {
		try {
			tls.reset();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}	
	}

	@Override
	public void post() {
		try {
			tls.closeSocket();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}



	
}
