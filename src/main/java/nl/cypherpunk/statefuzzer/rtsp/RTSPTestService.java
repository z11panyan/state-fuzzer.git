package nl.cypherpunk.statefuzzer.rtsp;


import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class RTSPTestService {
    Socket socket;
    OutputStream output;
    InputStream input;

    String host = "127.0.0.1";
    int port = 8554;
    String hostname = "localhost";
    // Act as a TLS client
    boolean ROLE_CLIENT = true;
    // Restart server after every session
    boolean REQUIRE_RESTART = true;
    // Timeout in ms
    int RECEIVE_MSG_TIMEOUT = 50;
    // Send output from TLS implementation to console
    boolean CONSOLE_OUTPUT = false;

    String cmd;
    public Process targetProcess;

    String redirectFile;
    ArrayList<byte[]> currentCase;
    int totalExecs;
    int prevCoverage;
    String outputDir;
    String env_cmd="";
    PrintWriter out;
    BitSet coverageMap;
    Double initScore;
    int seq;
    String session;
    private static final String RTSP_OK = "RTSP/1.0";
    private static final String HTTP_OK = "HTTP/1.0";
    String trackInfo = "1";
    HashSet<String> crashHash;
    public RTSPTestService() throws Exception {

    }

    public void setTarget(String target) throws Exception {
        if(target.equals("server")) {
            ROLE_CLIENT = true;
        }
        else if(target.equals("client")) {
            ROLE_CLIENT = false;
        }
        else {
            throw new Exception("Unknown target");
        }
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setCommand(String cmd) {
        this.cmd = cmd;
    }

    public void setRestartTarget(boolean restart) {
        this.REQUIRE_RESTART = restart;
    }

    public void setReceiveMessagesTimeout(int timeout) {
        RECEIVE_MSG_TIMEOUT = timeout;
    }

    public void setConsoleOutput(boolean enable) {
        CONSOLE_OUTPUT = enable;
    }

    public void setOutputDir(String dir){
        this.outputDir = dir;
    }
    public void setRedirectFile(String output) {
        redirectFile = output;
    }

    public void start() throws Exception {
        currentCase = new ArrayList<>();
        prevCoverage = 0;
        totalExecs = 0;
        crashHash = new HashSet<>();
        if(ROLE_CLIENT) {

            if(cmd != null && !cmd.equals("")) {

                ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
                pb.redirectErrorStream(true);
                //pb.redirectOutput(new File(redirectFile));
                targetProcess = pb.start();
                Thread.sleep(200);
            }
            session = "";
            seq = 1;
        }
        else {
            RTSPTestService.RTSPTestServiceRunnable rtspTestService = this.new RTSPTestServiceRunnable(this);
            rtspTestService.start();
            Thread.sleep(50);
            if(cmd != null && !cmd.equals("")) {
                ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
                pb.redirectErrorStream(true);
                //pb.redirectOutput(new File(redirectFile));
                Map< String,String> env = pb.environment();
                env.put("LD_PRELOAD",env_cmd);

				if(CONSOLE_OUTPUT) {
					pb.inheritIO();
				}
				/*
				else {
					pb.redirectErrorStream(true);
					pb.redirectOutput(new File("output.log"));
				}
				*/
                targetProcess = pb.start();
            }
            while(!rtspTestService.isReady()) Thread.sleep(10);

        }
    }
    public void reset() throws Exception {
        //System.out.println("RESET");
        if(socket!=null)
            socket.close();
        totalExecs ++;
        if(totalExecs%1000==0)
            System.out.println(totalExecs);
        if(ROLE_CLIENT) {
            if (!currentCase.isEmpty()) {
                int coverage = getCoverage();
                //reader.close();
                if (coverage == 1)
                    seedSave(currentCase, this.outputDir + File.separator + "Crash" + File.separator + "Crash_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));
                //if (coverage == 2)
                //    seedSave(currentCase, this.outputDir + File.separator + "Seed" + File.separator + "Abnormal_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));
                if (coverage > prevCoverage) {
                    System.out.println(coverage);
                    seedSave(currentCase, this.outputDir + File.separator + "Seed" + File.separator + "A_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));

                    prevCoverage = coverage;
                }
                currentCase.clear();
            }
            if (REQUIRE_RESTART && cmd != null && !cmd.equals("")) {
                targetProcess.destroy();
                //String target = this.outputDir.substring(0, this.outputDir.indexOf("2"));
                String killall = "sudo killall testOnDemandRTSPServer";
                if (REQUIRE_RESTART) {
                    ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
                    Process tmp = pbkill.start();
                    tmp.waitFor();
                }
                Thread.sleep(50);
                ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
                pb.redirectErrorStream(true);
                //File log = new File("log.txt");
                //pb.redirectOutput(new File(redirectFile));
                targetProcess = pb.start();

                Thread.sleep(100);
            }
            //Thread.sleep(100);
            try {
                connectSocket();
            }
            catch (Exception e)
            {
                String killall = "sudo killall testOnDemandRTSPServer";
                if (REQUIRE_RESTART) {
                    ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
                    Process tmp = pbkill.start();
                    tmp.waitFor();
                }
                Thread.sleep(200);
                ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
                pb.redirectErrorStream(true);
                //File log = new File("log.txt");
                //pb.redirectOutput(new File(redirectFile));
                targetProcess = pb.start();

                Thread.sleep(500);
                connectSocket();
            }
            seq = 1;
            session = "";
        } else {
            if (!currentCase.isEmpty()) {
                int coverage = getCoverage();
                //reader.close();
                if (coverage == 1)
                    seedSave(currentCase, this.outputDir + File.separator + "Crash" + File.separator + "Crash_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));
                //if (coverage == 2)
                //    seedSave(currentCase, this.outputDir + File.separator + "Seed" + File.separator + "Abnormal_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));
                if (coverage > prevCoverage) {
                    System.out.println(coverage);
                    seedSave(currentCase, this.outputDir + File.separator + "Seed" + File.separator + "A_" + String.format("%06d", totalExecs) + "_" + String.format("%05d", coverage));

                    prevCoverage = coverage;
                }
                currentCase.clear();
            }
            String killall = "sudo killall testRTSPClient";
            ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
            Process tmp = pbkill.start();
            tmp.waitFor();
            Thread.sleep(50);
            RTSPTestService.RTSPTestServiceRunnable rtspTestService = this.new RTSPTestServiceRunnable(this);
            rtspTestService.start();
            Thread.sleep(50);
            if(cmd != null && !cmd.equals("")) {
                ProcessBuilder pb = new ProcessBuilder(cmd.split(" "));
                pb.redirectErrorStream(true);
                //File log = new File(redirectFile);
                //pb.redirectOutput(new File(redirectFile));
                Map< String,String> env = pb.environment();
                env.put("LD_PRELOAD",env_cmd);

				if(CONSOLE_OUTPUT) {
					pb.inheritIO();
				}
				/*
				else {
					pb.redirectErrorStream(true);
					pb.redirectOutcrashHashput(new File("output.log"));
				}
				*/
                targetProcess = pb.start();
                Thread.sleep(50);

                session = "";
                receiveMessages();
            }
        }
    }

    public String receiveMessages() throws Exception {
        String out = "";
        String statusCode = "";
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if(socket.isInputShutdown())
            return "ConnectionClosed";
        try {
            byte[] buffer = new byte[1024];
            int len = 0;
            while((len = input.read(buffer)) != -1) {
                if(len==0)
                    break;
                bos.write(buffer, 0, len);
                //if(bos.toString().endsWith("\r\n\r\n"))
                //    break;
            }
            bos.close();
            out = bos.toString();
        }
        catch (SocketTimeoutException e)
        {
            //socket.close();
            bos.close();
            if(bos.size()==0)
                out = "Empty";
            else
                out = bos.toString();
        }
        String[] tmp = out.split("\r\n\r\nRTSP");
        for (int i = 0; i < tmp.length; i++) {
            String reponse = "";
            if(i==0)
                reponse = tmp[0];
            else
                reponse = "RTSP"+tmp[i];
            if (reponse.startsWith(RTSP_OK)) {
                statusCode = statusCode + reponse.substring(reponse.indexOf(RTSP_OK)+9,reponse.indexOf(RTSP_OK)+12);
                if(reponse.contains("trackID"))
                    trackInfo=reponse.substring(reponse.indexOf("trackID"));
                else if(reponse.contains("Session:"))
                    session = reponse.substring(reponse.indexOf("Session: ") + 9, reponse.indexOf("Session: ") + 17);
            } else if (reponse.startsWith(HTTP_OK)) {
                statusCode = statusCode + reponse.substring(reponse.indexOf(HTTP_OK)+9,reponse.indexOf(HTTP_OK)+12);
                if(reponse.contains("trackID"))
                    trackInfo=reponse.substring(reponse.indexOf("trackID"));
                else if(reponse.contains("Session:"))
                    session = reponse.substring(reponse.indexOf("Session: ") + 9, reponse.indexOf("Session: ") + 17);
            }
            else if (reponse.startsWith("SETUP")) {
                if(reponse.contains("track1"))
                    statusCode = statusCode + "SETUP1";
                else if(reponse.contains("track2"))
                    statusCode = statusCode + "SETUP2";
            } else if (reponse.startsWith("PLAY")) {
                statusCode = statusCode + "PLAY";
            } else if (reponse.startsWith("TEARDOWN")) {
                statusCode = statusCode + "TEARDOWN";
            } else if (reponse.startsWith("DESCRIBE")) {
                statusCode = statusCode + "DESCRIBE";
            }
            else {
                //if(reponse.length() > 4)
                //    System.out.println(reponse.substring(0,4));
                statusCode = "ConnectionClosed";
            }

            if(reponse.contains("ConnectionClosed") && ROLE_CLIENT) {
                try {
                    if (!socket.isOutputShutdown()) {
                        String message = buildTeardown();
                        message = message.concat("\r\n");
                        sendMessage(message.getBytes("US-ASCII"));
                        socket.close();
                    }
                } catch (Exception e)
                {
                    socket.close();
                }
            }
        }

        return statusCode;
    }



    public String processSymbol(String input) throws Exception {
        String inAction = input;
        String out = "";
        if(!socket.isConnected() || socket.isClosed()) return "ConnectionClosed";

        try {
            if (inAction.equals("OPTIONS")) {
                out = buildOptions();
            } else if(inAction.equals("DESCRIBE")){
                out = buildDescribe();
            } else if(inAction.equals("SETUP1")){
                out = buildSetup1();
            } else if(inAction.equals("SETUP2")){
                out = buildSetup2();
            } else if(inAction.equals("PLAY")){
                out = buildPlay();
            } else if(inAction.equals("PAUSE")){
                out = buildPause();
            } else if(inAction.equals("TEARDOWN")){
                out = buildTeardown();
            } else if(inAction.equals("GETPARAMETER")){
                out = buildGET_PARAMETER();
            } else if(inAction.equals("S_DESCRIBE")){
                out = buildServerDescribe();
            } else if(inAction.equals("S_SETUP1")){
                out = buildServerSetup1();
            } else if(inAction.equals("S_SETUP2")){
                out = buildServerSetup2();
            } else if(inAction.equals("S_PLAY")){
                out = buildServerPlay();
            } else if(inAction.equals("S_TEARDOWN")){
                out = buildServerTeardown();
            } else if(inAction.equals("Session_NotFound")){
                out = buildSessionNotFound();
            } else if(inAction.equals("Stream_NotFound")){
                out = buildStreamNotFound();
            } else if(inAction.equals("BadRequest")){
                out = buildBadRequest();
            } else if(inAction.equals("Method_NotAllowed")){
                out = buildMethodNotAllowed();
            }
            out  = out.concat("\r\n");
            byte [] send = out.getBytes("US-ASCII");
            sendMessage(send);
            currentCase.add(send);
            seq ++;
            return receiveMessages();
        }
        catch(SocketException e) {
            //String outAction = "ConnectionClosedException";
            String outAction = "ConnectionClosed";

            return outAction;
        }
    }

    public String buildOptions() throws Exception {
        String message = "OPTIONS rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        if(!session.isEmpty())
            message = message.concat("Session: "+session+"\r\n");
        return message;
    }

    public String buildGET_PARAMETER() throws Exception{
        String message = "GET_PARAMETER * RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        if(!session.isEmpty())
            message = message.concat("Session: "+session+"\r\n");
        return message;
    }

    public String buildDescribe() throws Exception {
        String message = "DESCRIBE rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        message = message.concat("Accept: application/sdp\r\n");
        return message;
    }

    public String buildSetup1() throws Exception {
        String message = "SETUP rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest/track1"+" RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        //if(session.length()>0)
        //    message = message.concat("Session: "+session+"\r\n");
        message = message.concat("Transport: RTP/AVP;unicast;client_port=37216-37217\r\n");
        return message;
    }

    public String buildSetup2() throws Exception {
        String message = "SETUP rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest/track2"+" RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        //if(session.length()>0)
        message = message.concat("Session: 000022B8\r\n");
        message = message.concat("Transport: RTP/AVP;unicast;client_port=37216-37217\r\n");
        return message;
    }

    public String buildPlay() throws Exception {
        String message = "PLAY rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest/ RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        message = message.concat("Session: 000022B8\r\n");
        message = message.concat("Range: npt=0.000-\r\n");
        return message;
    }
    //
    public String buildPause() throws Exception {
        String message = "PAUSE rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest/ RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        message = message.concat("Session: 000022B8\r\n");
        return message;
    }

    public String buildTeardown() throws Exception {
        String message = "TEARDOWN rtsp://"+host+":"+Integer.toString(port)+"/aacAudioTest/ RTSP/1.0\r\n";
        message = message.concat("CSeq: "+Integer.toString(seq)+"\r\n");
        message = message.concat("User-Agent: ./testRTSPClient (LIVE555 Streaming Media v2018.08.28)\r\n");
        message = message.concat("Session: 000022B8\r\n");
        return message;
    }



    public String buildServerDescribe() throws Exception{
        String message = "RTSP/1.0 200 OK\r\n" +
                "CSeq: 2\r\n" +
                "Date: Thu, Oct 21 2021 02:05:03 GMT\r\n" +
                "Content-Base: rtsp://127.0.0.1:8554/matroskaFileTest/\r\n" +
                "Content-Type: application/sdp\r\n" +
                "Content-Length: 744\r\n" +
                "\r\n" +
                "v=0\r\n" +
                "o=- 1634781798495333 1 IN IP4 192.168.10.160\r\n" +
                "s=Session streamed by \"testOnDemandRTSPServer\"\r\n" +
                "i=matroskaFileTest\r\n" +
                "t=0 0\r\n" +
                "a=tool:LIVE555 Streaming Media v2018.08.28\r\n" +
                "a=type:broadcast\r\n" +
                "a=control:*\r\n" +
                "a=range:npt=0-5.570\r\n" +
                "a=x-qt-text-nam:Session streamed by \"testOnDemandRTSPServer\"\r\n" +
                "a=x-qt-text-inf:matroskaFileTest\r\n" +
                "m=video 0 RTP/AVP 96\r\n" +
                "c=IN IP4 0.0.0.0\r\n" +
                "b=AS:500\r\n" +
                "a=rtpmap:96 H264/90000\r\n" +
                "a=fmtp:96 packetization-mode=1;profile-level-id=42C01E;sprop-parameter-sets=Z0LAHtkAjCmhAAADAAEAAAMAPA8WLkg=,aMuBcsg=\r\n" +
                "a=control:track1\r\n" +
                "m=audio 0 RTP/AVP 97\r\n" +
                "c=IN IP4 0.0.0.0\r\n" +
                "b=AS:96\r\n" +
                "a=rtpmap:97 MPEG4-GENERIC/44100\r\n" +
                "a=fmtp:97 streamtype=5;profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1208\r\n" +
                "a=control:track2";
        return message;
    }

    public String buildServerSetup1() throws Exception{
        String message = "RTSP/1.0 200 OK\r\n" +
                "CSeq: 3\r\n" +
                "Date: Thu, Oct 21 2021 02:05:03 GMT\r\n" +
                "Transport: RTP/AVP;unicast;destination=127.0.0.1;source=127.0.0.1;client_port=37216-37217;server_port=6970-6971\r\n" +
                "Session: 000022B8;timeout=65\r\n";
        return message;
    }

    public String buildServerSetup2() throws Exception{
        String message = "RTSP/1.0 200 OK\r\n" +
                "CSeq: 4\r\n" +
                "Date: Thu, Oct 21 2021 02:05:03 GMT\r\n" +
                "Transport: RTP/AVP;unicast;destination=127.0.0.1;source=127.0.0.1;client_port=49022-49023;server_port=6972-6973\r\n" +
                "Session: 000022B8;timeout=65\r\n";
        return message;
    }

    public String buildServerPlay() throws Exception{
        String message = "RTSP/1.0 200 OK\r\n" +
                "CSeq: 5\r\n" +
                "Date: Thu, Oct 21 2021 02:05:03 GMT\r\n" +
                "Range: npt=0.000-\r\n" +
                "Session: 000022B8\r\n" +
                "RTP-Info: url=rtsp://127.0.0.1:8554/matroskaFileTest/track1;seq=37442;rtptime=1515896127,url=rtsp://127.0.0.1:8554/matroskaFileTest/track2;seq=63317;rtptime=1590094704\r\n";
        return message;
    }

    public String buildServerTeardown() throws Exception{
        String message = "RTSP/1.0 200 OK\n" +
                "CSeq: 6\n" +
                "Date: Thu, Oct 21 2021 02:05:04 GMT\r\n";
        return message;
    }

    public String buildStreamNotFound() throws Exception{
        String message = "RTSP/1.0 404 Stream Not Found\r\n" +
                "CSeq: 2\r\n" +
                "Date: Thu, Oct 21 2021 15:33:28 GMT\r\n";
        return message;
    }

    public String buildSessionNotFound() throws Exception{
        String message = "RTSP/1.0 454 Session Not Found\r\n" +
                "CSeq: 1\r\n" +
                "Date: Thu, Oct 21 2021 15:37:39 GMT\r\n";
        return message;
    }

    public String buildBadRequest() throws Exception{
        String message = "RTSP/1.0 400 Bad Request\r\n" +
                "Date: Mon, Oct 25 2021 02:02:43 GMT\r\n" +
                "Allow: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE, GET_PARAMETER, SET_PARAMETER\r\n";
        return message;
    }


    public String buildMethodNotAllowed() throws Exception{
        String message = "RTSP/1.0 405 Method Not Allowed\r\n" +
                "CSeq: 2\r\n" +
                "Date: Mon, Oct 25 2021 02:02:38 GMT\r\n" +
                "Allow: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE, GET_PARAMETER, SET_PARAMETER\r\n";
        return message;
    }

    public String sendFuzzingMessage (byte[] message) throws Exception
    {
        try {
            if (socket.isClosed())
                return "ConnectionClosed";
            output.write(message);
            return receiveFuzzingMessages();
        }
        catch(SocketException e) {
            //String outAction = "ConnectionClosedException";
            String outAction = "ConnectionClosed";

            return outAction;
        }
    }

    public String receiveFuzzingMessages() throws Exception {
        try{
            String out = receiveMessages();
            if(out.contains("ConnectionClosed")) {
                socket.close();
            }
            return out;
        }
        catch(SocketException e) {
            //String outAction = "ConnectionClosedException";
            String outAction = "ConnectionClosed";

            return outAction;
        }

    }

    public String connectSocket() throws UnknownHostException, IOException,Exception {
        socket = new Socket(host, port);
        socket.setTcpNoDelay(true);
        socket.setSoTimeout(RECEIVE_MSG_TIMEOUT);
        socket.setKeepAlive(false);
        socket.setReuseAddress(true);
        output = socket.getOutputStream();
        input = socket.getInputStream();
        return "";
    }

    public void listenSocket() throws UnknownHostException, IOException {
        ServerSocket server = new ServerSocket();
        server.bind(new InetSocketAddress(host, port));
        socket = server.accept();
        socket.setTcpNoDelay(true);
        socket.setSoTimeout(RECEIVE_MSG_TIMEOUT);

        output = socket.getOutputStream();
        input = socket.getInputStream();

        server.close();
    }

    public void closeSocket() throws IOException, Exception {
        socket.close();
    }
    void sendMessage(byte[] msg) throws Exception {
        if(!socket.isOutputShutdown())
            output.write(msg);
    }

    public void close() {
        if(targetProcess != null) {
            targetProcess.destroy();
        }
    }

    class RTSPTestServiceRunnable extends Thread {
        RTSPTestService rtsp;
        boolean ready;

        public RTSPTestServiceRunnable(RTSPTestService rtsp) {
            ready = false;
            this.rtsp = rtsp;
        }

        public boolean isReady() {
            return ready;
        }

        public boolean isConnected() {
            return rtsp.socket.isConnected();
        }

        public boolean isBound() {
            return  (rtsp.socket != null) && rtsp.socket.isBound();
        }

        public void run() {
            try {
                rtsp.listenSocket();
                rtsp.receiveMessages();
                ready = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //change by pany
    public void seedSave(List<byte[]> seed, String filename) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            file = new File(filename);
            if (file.exists()) {
                return;
            }
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            for (int i = 0; i < seed.size(); i++) {
                byte[] tmp = intToByteArray(seed.get(i).length);
                bos.write(tmp, 0, 4);
                bos.write(seed.get(i), 0, seed.get(i).length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public static byte[] intToByteArray(int i) {
        byte[] result = new byte[4];
        result[3] = (byte)((i >> 24) & 0xFF);
        result[2] = (byte)((i >> 16) & 0xFF);
        result[1] = (byte)((i >> 8) & 0xFF);
        result[0] = (byte)(i & 0xFF);
        return result;
    }

    public int getCoverage() throws IOException {
        int coverage = 0;
        String resultStr = new String();
        /*if (ROLE_CLIENT) {
            //BufferedReader reader = new BufferedReader(new InputStreamReader(targetProcess.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;

            while (reader.ready()) {
                line = reader.readLine();
                if (line == null)
                    break;
                if (line.contains("Coverage")) {
                    try {
                        int index = line.indexOf("Coverage");
                        String cover = line.substring(index + 9).trim();
                        coverage = Integer.parseInt(cover);
                        //break;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                if (line.contains("AddressSanitizer"))
                    coverage = 1;
                resultStr = resultStr.concat(line);
            }
        } else {*/
        if (coverageMap == null)
            coverage = 0;
        else
            coverageMap.cardinality();
        BufferedReader reader = new BufferedReader(new InputStreamReader(targetProcess.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        try {
            //Thread.sleep(100);
            while (reader.ready()) {
                line = reader.readLine();
                if (line == null)
                    break;
                if (line.contains("Bitmap")) {
                    try {
                        int index = line.indexOf("Bitmap");
                        String cover = line.substring(index + 8).trim();
                        if (coverageMap == null) {
                            coverageMap = new BitSet(cover.length());
                        }
                        for (int i = 0; i < cover.length(); i++)
                            if (cover.charAt(i) == '1')
                                coverageMap.set(i);
                        coverage = coverageMap.cardinality();
                        //System.out.print(line);
                        //break;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                if (line.contains("AddressSanitizer") && !line.contains("SUMMARY: AddressSanitizer:")) {
                    //System.out.println(line);
                    Thread.sleep(500);
                }
                if (line.contains("SUMMARY: AddressSanitizer:")) {
                    coverage = 1;
                    //System.out.println(line);
                    if (!crashHash.add(line))
                        return 2;
                    resultStr = resultStr.concat(line + "\r\n");
                    System.out.println(resultStr);
                    return coverage;
                }
                if (line.contains("Aborted")) {
                    coverage = 1;
                    if (!crashHash.add(line))
                        return 2;
                    System.out.println(line);
                    return coverage;
                }
                resultStr = resultStr.concat(line + "\r\n");
                //System.out.print(line);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        //}
        if (resultStr.length() == 0)
            coverage = 3;
        //System.out.print(resultStr);
        if (ROLE_CLIENT){
            if (targetProcess.isAlive())
                return coverage;
            else {
                System.out.println("Aborted");
                return 1;
            }
        }
        return coverage;
    }
}
