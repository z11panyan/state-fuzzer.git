package nl.cypherpunk.statefuzzer.rtsp;

import nl.cypherpunk.statefuzzer.LearningConfig;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RTSPConfig extends LearningConfig{
    String alphabet;

    String target;
    String cmd;
    String cmd_version;
    String version;
    int diffnum;
    List<String> cmdList;
    List<Integer> portList;
    String host;
    int port;

    boolean restart;
    boolean console_output;
    int timeout;
    //change by pany
    public String output_dir;
    public String redirectFile;
    //public String coverage_cmd;

    public RTSPConfig(String filename) throws IOException {
        super(filename);
    }

    public RTSPConfig(LearningConfig config) {
        super(config);
    }

    @Override
    public void loadProperties() {
        super.loadProperties();

        if (properties.getProperty("alphabet") != null)
            alphabet = properties.getProperty("alphabet");

        if (properties.getProperty("target").equalsIgnoreCase("client") || properties.getProperty("target").equalsIgnoreCase("server"))
            target = properties.getProperty("target").toLowerCase();

        if (properties.getProperty("cmd") != null)
            cmd = properties.getProperty("cmd");

        if (properties.getProperty("cmd_version") != null)
            cmd_version = properties.getProperty("cmd_version");

        if (properties.getProperty("host") != null)
            host = properties.getProperty("host");

        if (properties.getProperty("port") != null)
            port = Integer.parseInt(properties.getProperty("port"));

        if (properties.getProperty("console_output") != null)
            console_output = Boolean.parseBoolean(properties.getProperty("console_output"));
        else
            console_output = false;

        if (properties.getProperty("restart") != null)
            restart = Boolean.parseBoolean(properties.getProperty("restart"));
        else
            restart = false;

        if (properties.getProperty("timeout") != null)
            timeout = Integer.parseInt(properties.getProperty("timeout"));
        //change by  pany
        if (properties.getProperty("output_dir") != null) {
            redirectFile = super.output_dir + File.separator + "server.log";
            output_dir = super.output_dir;
        } else
            redirectFile = "output_server/server.log";

        if(properties.getProperty("diffnum") != null)
        {
            diffnum = Integer.parseInt(properties.getProperty("diffnum"));
            cmdList = new ArrayList<>();
            portList = new ArrayList<>();
            for(int i = 0; i < diffnum; i++)
            {
                cmdList.add(properties.getProperty("cmd"+String.valueOf(i+1)));
                portList.add(Integer.parseInt(properties.getProperty("port"+String.valueOf(i+1))));
            }
        }
    }
}
