/*
 *  Copyright (c) 2016 Joeri de Ruiter
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package nl.cypherpunk.statefuzzer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Configuration class used for learning parameters
 * 
 * @author Joeri de Ruiter (joeri@cs.ru.nl)
 */
public class LearningConfig {
	public static int TYPE_SMTP = 1;
	public static int TYPE_RTSP = 2;
	public static int TYPE_TLS = 3;
	public static int TYPE_VPN = 4;
	protected Properties properties;
	
	public int type = TYPE_VPN;
	

	
	String learning_algorithm = "lstar";
	String eqtest = "randomwords";
	
	// Used for W-Method and Wp-method
	int max_depth = 10;
	
	// Used for Random words
	int min_length = 5;
	int max_length = 10;
	int nr_queries = 100;
	int seed = 1;
	// change by pany
	//public String coverage_cmd;
	public String output_dir = "";
	public String seed_dir;
	public String version;
	// change by pany
	public int diffnum;
	public List<String> cmdList;
	public List<Integer> portList;
	public List<String> versionList;
	public LearningConfig(String filename) throws IOException {
		properties = new Properties();

		InputStream input = new FileInputStream(filename);
		properties.load(input);

		loadProperties();
	}
	
	public LearningConfig(LearningConfig config) {
		properties = config.getProperties();
		if(config.output_dir.length()>15)
			properties.setProperty("output_dir",config.output_dir);
		loadProperties();
	}
	
	public Properties getProperties() {
		return properties;
	}

	public void loadProperties() {
		// change by pany
		if(properties.getProperty("output_dir").length() < 15)
		{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
			output_dir = properties.getProperty("output_dir") + df.format(new Date());
		}
		else
		{
			output_dir = properties.getProperty("output_dir");
		}

		if(properties.getProperty("type") != null) {
			if(properties.getProperty("type").equalsIgnoreCase("smtp"))
				type = TYPE_SMTP;
			else if(properties.getProperty("type").equalsIgnoreCase("tls"))
				type = TYPE_TLS;
			else if(properties.getProperty("type").equalsIgnoreCase("vpn"))
				type = TYPE_VPN;
			else if(properties.getProperty("type").equalsIgnoreCase("rtsp"))
				type = TYPE_RTSP;
		}
		
		if(properties.getProperty("learning_algorithm").equalsIgnoreCase("lstar") || properties.getProperty("learning_algorithm").equalsIgnoreCase("dhc") || properties.getProperty("learning_algorithm").equalsIgnoreCase("kv") || properties.getProperty("learning_algorithm").equalsIgnoreCase("ttt") || properties.getProperty("learning_algorithm").equalsIgnoreCase("mp") || properties.getProperty("learning_algorithm").equalsIgnoreCase("rs"))
			learning_algorithm = properties.getProperty("learning_algorithm").toLowerCase();
		
		if(properties.getProperty("eqtest") != null && (properties.getProperty("eqtest").equalsIgnoreCase("wmethod") || properties.getProperty("eqtest").equalsIgnoreCase("modifiedwmethod") || properties.getProperty("eqtest").equalsIgnoreCase("wpmethod") || properties.getProperty("eqtest").equalsIgnoreCase("randomwords")))
			eqtest = properties.getProperty("eqtest").toLowerCase();
		
		if(properties.getProperty("max_depth") != null)
			max_depth = Integer.parseInt(properties.getProperty("max_depth"));
		
		if(properties.getProperty("min_length") != null)
			min_length = Integer.parseInt(properties.getProperty("min_length"));
		
		if(properties.getProperty("max_length") != null)
			max_length = Integer.parseInt(properties.getProperty("max_length"));
		
		if(properties.getProperty("nr_queries") != null)
			nr_queries = Integer.parseInt(properties.getProperty("nr_queries"));
		
		if(properties.getProperty("seed") != null)
			seed = Integer.parseInt(properties.getProperty("seed"));
		/*
		if(properties.getProperty("coverage_cmd") != null)
			coverage_cmd = properties.getProperty("coverage_cmd");
		*/
		// change by pany
		if(properties.getProperty("seed_dir") != null)
			seed_dir = properties.getProperty("seed_dir");

		if(properties.getProperty("version") != null)
			version = properties.getProperty("version");

		if(properties.getProperty("diffnum") != null)
		{
			diffnum = Integer.parseInt(properties.getProperty("diffnum"));
			cmdList = new ArrayList<>();
			portList = new ArrayList<>();
			versionList = new ArrayList<>();
			for(int i = 0; i < diffnum; i++)
			{
				cmdList.add(properties.getProperty("cmd"+String.valueOf(i+1)));
				portList.add(Integer.parseInt(properties.getProperty("port"+String.valueOf(i+1))));
				versionList.add(properties.getProperty("version"+String.valueOf(i+1)));
			}
		}
	}
}
