/*
 *  Copyright (c) 2017 Lesly-Ann Daniel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package nl.cypherpunk.statefuzzer.openvpn.messages;

import java.io.ByteArrayInputStream;

public class SoftResetV1 extends Control {

	public SoftResetV1(ByteArrayInputStream input) {
		super(input);
	}
	
	public SoftResetV1(byte[] sessionId, byte[] packetId) {
		super(sessionId, packetId);
	}
}
