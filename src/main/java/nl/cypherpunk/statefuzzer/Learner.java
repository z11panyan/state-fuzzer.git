/*
 *  Copyright (c) 2016 Joeri de Ruiter
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package nl.cypherpunk.statefuzzer;

import de.learnlib.acex.AcexAnalyzer;
import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.dhc.mealy.MealyDHC;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealyBuilder;
import de.learnlib.algorithms.malerpnueli.MalerPnueliMealy;
import de.learnlib.algorithms.rivestschapire.RivestSchapireMealy;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.SUL;
import de.learnlib.api.algorithm.LearningAlgorithm;
import de.learnlib.api.logging.LearnLogger;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.counterexamples.AcexLocalSuffixFinder;
import de.learnlib.filter.cache.mealy.MealyCacheOracle;
import de.learnlib.filter.cache.mealy.MealyCaches;
import de.learnlib.filter.statistic.Counter;
import de.learnlib.filter.statistic.oracle.MealyCounterOracle;
import de.learnlib.oracle.equivalence.*;
import de.learnlib.oracle.membership.SULOracle;
import de.learnlib.oracle.membership.SimulatorOracle;
import de.learnlib.util.statistics.SimpleProfiler;
import net.automatalib.automata.transducers.MealyMachine;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import nl.cypherpunk.statefuzzer.LogOracle.MealyLogOracle;
import nl.cypherpunk.statefuzzer.ModifiedWMethodEQOracle.MealyModifiedWMethodEQOracle;
import nl.cypherpunk.statefuzzer.openvpn.VPNConfig;
import nl.cypherpunk.statefuzzer.openvpn.VPNSUL;
import nl.cypherpunk.statefuzzer.rtsp.RTSPConfig;
import nl.cypherpunk.statefuzzer.rtsp.RTSPSUL;
import nl.cypherpunk.statefuzzer.smtp.SMTPConfig;
import nl.cypherpunk.statefuzzer.smtp.SMTPSUL;
import nl.cypherpunk.statefuzzer.tls.TLSConfig;
import nl.cypherpunk.statefuzzer.tls.TLSSUL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

/**
 * @author Joeri de Ruiter (joeri@cs.ru.nl)
 */
public class Learner {
	LearningConfig config;
	Alphabet<String> alphabet;
	boolean combine_query = false;
	SUL<String, String> sul;
	SULOracle<String, String> memOracle;
	MealyLogOracle<String, String> logMemOracle;
	MealyCounterOracle<String, String> statsMemOracle;
	MealyCacheOracle<String, String> cachedMemOracle;
	MealyCounterOracle<String, String> statsCachedMemOracle;
	LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learningAlgorithm;

	SULOracle<String, String> eqOracle;
	MealyLogOracle<String, String> logEqOracle;
	MealyCounterOracle<String, String> statsEqOracle;
	MealyCacheOracle<String, String> cachedEqOracle;
	MealyCounterOracle<String, String> statsCachedEqOracle;
	EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> equivalenceAlgorithm;
	
	public Learner(LearningConfig config) throws Exception {
		this.config = config;
		
		// Create output directory if it doesn't exist
		Path path = Paths.get(config.output_dir);
		if(Files.notExists(path)) {
			Files.createDirectories(path);
		}
		
		configureLogging(config.output_dir);
		
		LearnLogger log = LearnLogger.getLogger(Learner.class.getSimpleName());

		// Check the type of learning we want to do and create corresponding configuration and SUL
		if(config.type == LearningConfig.TYPE_TLS) {
			log.logPhase( "Using TLS SUL");

			// Create the TLS SUL
			sul = new TLSSUL(new TLSConfig(config));
			alphabet = ((TLSSUL)sul).getAlphabet();			
		}
		else if(config.type == LearningConfig.TYPE_VPN) {
			log.logPhase("Using VPN SUL");
			
			// Create the VPN SUL
			sul = new VPNSUL(new VPNConfig(config));
			alphabet = ((VPNSUL)sul).getAlphabet();			
		}
		else if(config.type == LearningConfig.TYPE_SMTP) {
			log.logPhase("Using SMTP SUL");
			String killall = "sudo killall exim";
			ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
			Process tmp = pbkill.start();
			tmp.waitFor();
			killall = "sudo rm /home/exim*";
			pbkill = new ProcessBuilder(killall.split(" "));
			tmp = pbkill.start();
			tmp.waitFor();
			// Create the SMTP SUL
			sul = new SMTPSUL(new SMTPConfig(config));
			alphabet = ((SMTPSUL)sul).getAlphabet();
		}
		else if(config.type == LearningConfig.TYPE_RTSP) {
			log.logPhase("Using RTSP SUL");

			// Create the SMTP SUL
			sul = new RTSPSUL(new RTSPConfig(config));
			alphabet = ((RTSPSUL)sul).getAlphabet();
		}


		
		loadLearningAlgorithm(config.learning_algorithm, alphabet, sul);
		loadEquivalenceAlgorithm(config.eqtest, alphabet, sul);
	}
	
	public void loadLearningAlgorithm(String algorithm, Alphabet<String> alphabet, SUL<String, String> sul) throws Exception {
		// Create the membership oracle
		//memOracle = new SULOracle<String, String>(sul);
		// Add a logging oracle
		logMemOracle = new MealyLogOracle<String, String>(sul, LearnLogger.getLogger("learning_queries"));
		// Count the number of queries actually sent to the SUL
		statsMemOracle = new MealyCounterOracle<String, String>(logMemOracle, "membership queries to SUL");
		// Use cache oracle to prevent double queries to the SUL
		cachedMemOracle = MealyCacheOracle.createDAGCacheOracle(alphabet, statsMemOracle);
        // Count the number of queries to the cache
		statsCachedMemOracle = new MealyCounterOracle<String, String>(cachedMemOracle, "membership queries to cache");
		
		// Instantiate the selected learning algorithm
		switch(algorithm.toLowerCase()) {
			case "lstar":
				learningAlgorithm = new ExtensibleLStarMealyBuilder<String, String>().withAlphabet(alphabet).withOracle(statsCachedMemOracle).create();
				break;
				 	
			case "dhc":
				learningAlgorithm = new MealyDHC<String, String>(alphabet, statsCachedMemOracle);
				break;
				
			case "kv":
				learningAlgorithm = new KearnsVaziraniMealy<String, String>(alphabet, statsCachedMemOracle, true, AcexAnalyzers.BINARY_SEARCH_BWD);
				break;
				
			case "ttt":
				AcexLocalSuffixFinder suffixFinder = new AcexLocalSuffixFinder(AcexAnalyzers.BINARY_SEARCH_BWD, true, "Analyzer");
				learningAlgorithm = new TTTLearnerMealy<String, String>(alphabet, statsCachedMemOracle,  AcexAnalyzers.LINEAR_FWD);
				break;
				
			case "mp":
				learningAlgorithm = new MalerPnueliMealy<String, String>(alphabet, statsCachedMemOracle);
				break;
				
			case "rs":
				learningAlgorithm = new RivestSchapireMealy<String, String>(alphabet, statsCachedMemOracle);
				break;

			default:
				throw new Exception("Unknown learning algorithm " + config.learning_algorithm);
		}		
	}
	
	public void loadEquivalenceAlgorithm(String algorithm, Alphabet<String> alphabet, SUL<String, String> sul) throws Exception {
		//TODO We could combine the two cached oracle to save some queries to the SUL
		// Create the equivalence oracle
		//eqOracle = new SULOracle<String, String>(sul);
		// Add a logging oracle
		logEqOracle = new MealyLogOracle<String, String>(sul, LearnLogger.getLogger("equivalence_queries"));
		// Add an oracle that counts the number of queries
		statsEqOracle = new MealyCounterOracle<String, String>(logEqOracle, "equivalence queries to SUL");
		// Use cache oracle to prevent double queries to the SUL
		cachedEqOracle = MealyCacheOracle.createDAGCacheOracle(alphabet, statsEqOracle);
        // Count the number of queries to the cache
		statsCachedEqOracle = new MealyCounterOracle<String, String>(cachedMemOracle, "equivalence queries to cache");

		// Instantiate the selected equivalence algorithm
		switch(algorithm.toLowerCase()) {
			case "wmethod":
				equivalenceAlgorithm = new MealyWMethodEQOracle<String, String>(statsCachedMemOracle,0,config.max_depth);
				break;

			case "modifiedwmethod":
				equivalenceAlgorithm = new ModifiedWMethodEQOracle.MealyModifiedWMethodEQOracle<String, String>(config.max_depth, statsCachedMemOracle);
				break;
				
			case "wpmethod":
				equivalenceAlgorithm = new MealyWpMethodEQOracle<String, String>(statsCachedMemOracle,0,config.max_depth);
				break;
				
			case "randomwords":
				equivalenceAlgorithm = new MealyRandomWordsEQOracle<String, String>(statsCachedMemOracle, config.min_length, config.max_length, config.nr_queries, new Random(config.seed));
				break;
				
			default:
				throw new Exception("Unknown equivalence algorithm " + config.eqtest);
		}	
	}
	
	public void learn() throws IOException, InterruptedException {
		LearnLogger log = LearnLogger.getLogger(Learner.class.getSimpleName());

		/*MealyLogOracle<String, String> logEqOracle = new MealyLogOracle<String, String>(this.sul, LearnLogger.getLogger("queries"));
		statsEqOracle = new MealyCounterOracle<String, String>(logEqOracle,"MembershipQuery");

		cachedMemOracle = MealyCaches.createTreeCache(this.alphabet, statsEqOracle);
		equivalenceAlgorithm = new ModifiedWMethodEQOracle.MealyModifiedWMethodEQOracle<String, String>(config.max_depth,cachedMemOracle);
		learningAlgorithm
				= new ExtensibleLStarMealyBuilder<String, String>().withAlphabet(alphabet).withOracle(cachedMemOracle).create();
		*/
		log.logPhase( "Using learning algorithm " + learningAlgorithm.getClass().getSimpleName());
		log.logPhase( "Using equivalence algorithm " + equivalenceAlgorithm.getClass().getSimpleName());

		log.logPhase( "Starting learning");
		
		SimpleProfiler.start("Total time");
		
		boolean learning = true;
		Counter round = new Counter("Rounds", "");

		round.increment();
		System.out.println("Starting round " + round.getCount());
		SimpleProfiler.start("Learning");
		learningAlgorithm.startLearning();
		SimpleProfiler.stop("Learning");

		MealyMachine<?, String, ?, String> hypothesis = learningAlgorithm.getHypothesisModel();
		DefaultQuery<String, Word<String>> tmpCE = null;
		while(learning) {
			// Write outputs
			writeDotModel(hypothesis, alphabet, config.output_dir + "/hypothesis_" + round.getCount() + ".dot");

			// Search counter-example
			System.out.println("Searching for counter-example");
			SimpleProfiler.start("Searching for counter-example");
			DefaultQuery<String, Word<String>> counterExample = equivalenceAlgorithm.findCounterExample(hypothesis, alphabet);	
			SimpleProfiler.stop("Searching for counter-example");

			if(counterExample == null) {
				// No counter-example found, so done learning
				learning = false;

				// Write outputs
				writeDotModel(hypothesis, alphabet, config.output_dir + "/learnedModel.dot");
				//writeAutModel(hypothesis, alphabet, config.output_dir + "/learnedModel.aut");
			}
			else {
				if(tmpCE==null)
					tmpCE = counterExample;
				else {
					if (tmpCE.toString().equals(counterExample.toString())) {
						// No counter-example found, so done learning
						learning = false;

						// Write outputs
						writeDotModel(hypothesis, alphabet, config.output_dir + "/learnedModel.dot");
						//writeAutModel(hypothesis, alphabet, config.output_dir + "/learnedModel.aut");
					}
					else
						tmpCE = counterExample;
				}
				// Counter example found, update hypothesis and continue learning
				System.out.println("Counter-example found: " + counterExample.toString());
				//TODO Add more logging
				round.increment();
				System.out.println("Starting round " + round.getCount());

				SimpleProfiler.start("Learning");
				learningAlgorithm.refineHypothesis(counterExample);
				SimpleProfiler.stop("Learning");

				hypothesis = learningAlgorithm.getHypothesisModel();
			}
		}

		SimpleProfiler.stop("Total time");
		
		// Output statistics
		System.out.println("-------------------------------------------------------");
		System.out.println( SimpleProfiler.getResults());
		System.out.println(round.getSummary());
		System.out.println(statsCachedMemOracle.getStatisticalData().getSummary());
		System.out.println( "States in final hypothesis: " + hypothesis.size());
		/*
		if(config.type == LearningConfig.TYPE_TLS) {
			TLSSUL tmp = (TLSSUL)sul;
			tmp.exit();
			String killssl = "sudo killall " + config.output_dir;

			ProcessBuilder pbkill = new ProcessBuilder(killssl.split(" "));
			Process killprocess = pbkill.start();
			killprocess.waitFor();
		}
		 */
	}
	
	public static void writeAutModel(MealyMachine<?, String, ?, String> model, Alphabet<String> alphabet, String filename) throws FileNotFoundException {
		// Make use of LearnLib's internal representation of states as integers
		@SuppressWarnings("unchecked")
		MealyMachine<Integer, String, ?, String> tmpModel = (MealyMachine<Integer, String, ?, String>) model;
		
		// Write output to aut-file
		File autFile = new File(filename);
		PrintStream psAutFile = new PrintStream(autFile);
		
		int nrStates = model.getStates().size();
		// Compute number of transitions, assuming the graph is complete
		int nrTransitions = nrStates * alphabet.size();
		
		psAutFile.println("des(" + model.getInitialState().toString() + "," + nrTransitions + "," + nrStates + ")");
		
		Collection<Integer> states = tmpModel.getStates();

		for(Integer state: states) {
			for(String input: alphabet) {
				String output = tmpModel.getOutput(state, input);
				Integer successor = tmpModel.getSuccessor(state, input);
				psAutFile.println("(" + state + ",'" + input + " / " + output + "', " + successor + ")");
			}
		}
		
		psAutFile.close();
	}
	
	public static void writeDotModel(MealyMachine<?, String, ?, String> model, Alphabet<String> alphabet, String filename) throws IOException, InterruptedException {
		// Write output to dot-file
		File dotFile = new File(filename);
		PrintStream psDotFile = new PrintStream(dotFile);
		GraphDOT.write(model, alphabet, psDotFile);
		psDotFile.close();
		
		//TODO Check if dot is available
		
		// Convert .dot to .pdf
		Runtime.getRuntime().exec("dot -Tpdf -O " + filename);
	}
	
	public void configureLogging(String output_dir) throws SecurityException, IOException {
		/*
		LearnLogger loggerLearnlib = LearnLogger.getLogger("de.learnlib");
		loggerLearnlib.setLevel(Level.ALL);
		FileHandler fhLearnlibLog = new FileHandler(output_dir + "/learnlib.log");
		loggerLearnlib.addHandler(fhLearnlibLog);
		fhLearnlibLog.setFormatter(new SimpleFormatter());
		
		LearnLogger loggerLearner = LearnLogger.getLogger(Learner.class.getSimpleName());
		loggerLearner.setLevel(Level.ALL);
		FileHandler fhLearnerLog = new FileHandler(output_dir + "/learner.log");
		loggerLearner.addHandler(fhLearnerLog);
		fhLearnerLog.setFormatter(new SimpleFormatter());
		loggerLearner.addHandler(new ConsoleHandler());
		
		LearnLogger loggerLearningQueries = LearnLogger.getLogger("learning_queries");
		loggerLearningQueries.setLevel(Level.ALL);
		FileHandler fhLearningQueriesLog = new FileHandler(output_dir + "/learning_queries.log");
		loggerLearningQueries.addHandler(fhLearningQueriesLog);
		fhLearningQueriesLog.setFormatter(new SimpleFormatter());
		loggerLearningQueries.addHandler(new ConsoleHandler());		

		LearnLogger loggerEquivalenceQueries = LearnLogger.getLogger("equivalence_queries");
		loggerEquivalenceQueries.setLevel(Level.ALL);
		FileHandler fhEquivalenceQueriesLog = new FileHandler(output_dir + "/equivalence_queries.log");
		loggerEquivalenceQueries.addHandler(fhEquivalenceQueriesLog);
		fhEquivalenceQueriesLog.setFormatter(new SimpleFormatter());
		loggerEquivalenceQueries.addHandler(new ConsoleHandler());
		 */
	}
	
	public static void main(String[] args) throws Exception {
		if(args.length < 1) {
			System.err.println("Invalid number of parameters");
			System.exit(-1);
		}
		
		LearningConfig config = new LearningConfig(args[0]);
	
		Learner learner = new Learner(config);
		learner.learn();
		
		System.exit(0);
	}
}
