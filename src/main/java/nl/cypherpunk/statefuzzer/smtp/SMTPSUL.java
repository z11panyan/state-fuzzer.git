package nl.cypherpunk.statefuzzer.smtp;

import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;
import nl.cypherpunk.statefuzzer.fuzzing.SUT;
import nl.cypherpunk.statefuzzer.tls.TLSConfig;
import nl.cypherpunk.statefuzzer.tls.TLSTestService;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class SMTPSUL implements SUT<String, String> {
    Alphabet<String> alphabet;
    public SMTPTestService smtp;
    public SMTPSUL(SMTPConfig config) throws Exception {
        alphabet = Alphabets.fromList(Arrays.asList(config.alphabet.split(" ")));

        smtp = new SMTPTestService();

        smtp.setTarget(config.target);
        smtp.setHost(config.host);
        smtp.setPort(config.port);
        smtp.setCommand(config.cmd);
        smtp.setRestartTarget(config.restart);
        smtp.setReceiveMessagesTimeout(config.timeout);
        smtp.setConsoleOutput(config.console_output);
        smtp.setRedirectFile(config.redirectFile);
        //tls.setCoverageCmd(config.coverage_cmd);
        smtp.setOutputDir(config.output_dir);
        smtp.setVersion(config.version);
        File outputDir = new File(smtp.outputDir + File.separator +"Seed");
        outputDir.mkdirs();
        smtp.start();
    }

    public SMTPSUL(SMTPConfig config, int i) throws Exception {
        alphabet = Alphabets.fromList(Arrays.asList(config.alphabet.split(" ")));

        smtp = new SMTPTestService();

        smtp.setTarget(config.target);
        smtp.setHost(config.host);
        smtp.setPort(config.portList.get(i));
        smtp.setCommand(config.cmdList.get(i));
        smtp.setRestartTarget(config.restart);
        smtp.setReceiveMessagesTimeout(config.timeout);
        smtp.setConsoleOutput(config.console_output);
        smtp.setRedirectFile(config.redirectFile);
        //tls.setCoverageCmd(config.coverage_cmd);
        smtp.setOutputDir(config.output_dir);
        smtp.setVersion(config.versionList.get(i));
        File outputDir = new File(smtp.outputDir + File.separator +"Seed");
        outputDir.mkdirs();
        smtp.start();
    }

    @Override
    public String messageToSymbol(byte[] message){
        String[] tmp = message.toString().split(" ");
        return tmp[0];
    }

    public Alphabet<String> getAlphabet() {
        return alphabet;
    }

    public void exit()
    {
        smtp.close();
    }

    public boolean canFork() {
        return false;
    }
    //change by pany
    public void setInitScore(Double score){
        smtp.initScore = score;
    }

    public int getCoverage() throws IOException {
        return smtp.getCoverage();
    }

    public String SendFuzzingMessage(byte[] message) throws Exception
    {
        return smtp.sendFuzzingMessage(message);
    }

    public byte[] getCurrent(){
        return smtp.currentCase.get(smtp.currentCase.size()-1);
    }

    @Override
    public String step(String symbol) {
        String result = null;
        try {
            result = smtp.processSymbol(symbol);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void pre() {
        try {
            smtp.reset();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void post() {
        try {
            smtp.closeSocket();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
