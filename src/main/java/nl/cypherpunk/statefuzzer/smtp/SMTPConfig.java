package nl.cypherpunk.statefuzzer.smtp;

import nl.cypherpunk.statefuzzer.LearningConfig;

import java.io.File;
import java.io.IOException;

public class SMTPConfig extends LearningConfig {
    String alphabet;

    String target;
    String cmd;
    String cmd_version;
    String version;
    String keystore_filename;
    String keystore_password;

    String host;
    int port;

    boolean restart;
    boolean console_output;
    int timeout;
    //change by pany
    public String output_dir;
    public String redirectFile;
    //public String coverage_cmd;

    public SMTPConfig(String filename) throws IOException {
        super(filename);
    }

    public SMTPConfig(LearningConfig config) {
        super(config);
    }

    @Override
    public void loadProperties() {
        super.loadProperties();

        if (properties.getProperty("alphabet") != null)
            alphabet = properties.getProperty("alphabet");

        if (properties.getProperty("target").equalsIgnoreCase("client") || properties.getProperty("target").equalsIgnoreCase("server"))
            target = properties.getProperty("target").toLowerCase();

        if (properties.getProperty("cmd") != null)
            cmd = properties.getProperty("cmd");

        if (properties.getProperty("cmd_version") != null)
            cmd_version = properties.getProperty("cmd_version");

        if (properties.getProperty("version") != null)
            version = properties.getProperty("version");

        if (properties.getProperty("host") != null)
            host = properties.getProperty("host");

        if (properties.getProperty("port") != null)
            port = Integer.parseInt(properties.getProperty("port"));

        if (properties.getProperty("console_output") != null)
            console_output = Boolean.parseBoolean(properties.getProperty("console_output"));
        else
            console_output = false;

        if (properties.getProperty("restart") != null)
            restart = Boolean.parseBoolean(properties.getProperty("restart"));
        else
            restart = false;

        if (properties.getProperty("timeout") != null)
            timeout = Integer.parseInt(properties.getProperty("timeout"));
        //change by  pany
        if (properties.getProperty("output_dir") != null) {
            redirectFile = super.output_dir + File.separator + "server.log";
            output_dir = super.output_dir;
        } else
            redirectFile = "output_server/server.log";
    }
}
