package nl.cypherpunk.statefuzzer.smtp;


import java.util.Base64;

public class Base64Utile_cc {
    public static String EncodeBase64(byte[] data)
    {
        return Base64.getEncoder().encodeToString(data);
    }

    /*
     *解密过程
     */
    public static String DecodeBase64(String str) throws Exception{
        byte [] base64decodedBytes  = Base64.getDecoder().decode(str);
        return (new String(base64decodedBytes, "utf-8"));
    }
}
