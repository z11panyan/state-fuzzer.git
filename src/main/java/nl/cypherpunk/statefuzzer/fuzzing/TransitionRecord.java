package nl.cypherpunk.statefuzzer.fuzzing;
import net.automatalib.words.Word;
import nl.cypherpunk.statefuzzer.mealyDot.MealyState;

import java.util.List;

final class TransitionRecord {
    final MealyState state;
    final Word<String> accessSequence;
    final Word<String> outputSequence;
    final List<byte[]> messageList;
    final List<MealyState> stateList;
    TransitionRecord(MealyState state, Word<String> accessSequence, Word<String> outputSequence,List<byte[]> messageList, List<MealyState> stateList) {
        this.state = state;
        this.accessSequence = accessSequence;
        this.outputSequence = outputSequence;
        this.messageList = messageList;
        this.stateList = stateList;
    }
}
