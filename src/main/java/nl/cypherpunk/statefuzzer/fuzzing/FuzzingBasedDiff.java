package nl.cypherpunk.statefuzzer.fuzzing;

import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import nl.cypherpunk.statefuzzer.LearningConfig;
import nl.cypherpunk.statefuzzer.mealyDot.MealyDot;
import nl.cypherpunk.statefuzzer.mealyDot.MealyState;
import nl.cypherpunk.statefuzzer.mealyDot.OutputTransition;
import nl.cypherpunk.statefuzzer.rtsp.RTSPConfig;
import nl.cypherpunk.statefuzzer.rtsp.RTSPSUL;
import nl.cypherpunk.statefuzzer.smtp.SMTPConfig;
import nl.cypherpunk.statefuzzer.smtp.SMTPSUL;
import nl.cypherpunk.statefuzzer.tls.TLSConfig;
import nl.cypherpunk.statefuzzer.tls.TLSSUL;
import static nl.cypherpunk.statefuzzer.Common.*;
import java.io.*;
import java.util.*;

public class FuzzingBasedDiff {
    MealyDot mealyAutoamta;
    MealyState initialState;
    MealyState sinkedState;
    Random rand = new Random();
    Alphabet<String> alphabet;
    Alphabet<String> meaning;
    ArrayList<SUT<String,String>> sut;
    int diffnum;
    Map<String, LinkedHashMap<byte[], Double>> seed;
    ArrayList<List<byte[]>> standardValue;
    ArrayList<Word<String>> standardKey;
    ArrayList<Boolean> standardStatus;
    ArrayList<Double> standardScore;
    PrintStream log;
    String server_output;
    String Seed;
    String Crash;
    String seed_dir;
    String coverage_cmd;
    int initCoverage = 0;
    int total_num = 0;
    int havoc_num = 50;
    boolean ROLE_CLIENT = false;
    ArrayList<byte[]> messagePool;
    ArrayList<Double> messageScore;
    LinkedHashSet<String> uniqueDiff;
    ArrayList<List<byte[]>> seedPool;
    ArrayList<Double> seedScore;
    ArrayList<Integer> prevCoverage;
    ArrayList<FuzzingPath> statePool;
    ArrayList<Double> stateScore;
    ArrayList<byte[]> dict;
    int start;
    int end;
    public FuzzingBasedDiff(LearningConfig config, String dotFileName) throws Exception {
        mealyAutoamta = new MealyDot(dotFileName);
        initialState = mealyAutoamta.getInitialState();
        sinkedState = mealyAutoamta.getSinkedState();
        diffnum = config.diffnum;
        sut = new ArrayList<>();
        if(config.type == LearningConfig.TYPE_RTSP)
        {
            String killall = "sudo killall testOnDemandRTSPServer";
            ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
            Process tmp = pbkill.start();
            tmp.waitFor();
            for(int i = 0; i < diffnum; i++)
            {
                sut.add(new RTSPSUL(new RTSPConfig(config),i));
            }
            alphabet = ((RTSPSUL)sut.get(0)).getAlphabet();
        }
        else if(config.type == LearningConfig.TYPE_TLS)
        {
            for(int i = 0; i < diffnum; i++)
            {
                sut.add(new TLSSUL(new TLSConfig(config),i));
            }
            alphabet = ((TLSSUL)sut.get(0)).getAlphabet();
        }
        else if(config.type == LearningConfig.TYPE_SMTP)
        {
            String killall = "sudo killall exim";
            ProcessBuilder pbkill = new ProcessBuilder(killall.split(" "));
            Process tmp = pbkill.start();
            tmp.waitFor();
            for(int i = 0; i < diffnum; i++)
            {
                sut.add(new SMTPSUL(new SMTPConfig(config),i));
            }
            alphabet = ((SMTPSUL)sut.get(0)).getAlphabet();
        }
        this.server_output = config.output_dir + File.separator + "server.log";
        File serverLog = new File(this.server_output);
        this.Seed = config.output_dir + File.separator + "Seed";
        this.Crash= config.output_dir + File.separator + "Crash";
        File outputDir = new File(Seed);
        outputDir.mkdirs();
        File crashDir = new File(this.Crash);
        crashDir.mkdirs();
        seed_dir = config.seed_dir;
        standardValue = new ArrayList<>();
        standardKey = new ArrayList<>();
        standardStatus = new ArrayList<>();
        standardScore = new ArrayList<>();
        seed = new HashMap<>();
        uniqueDiff = new LinkedHashSet<>();
        dict = new ArrayList<>();
        for (int i = 0; i < alphabet.size(); i++) {
            seed.put(alphabet.getSymbol(i), new LinkedHashMap<>());
        }
        statePool = new ArrayList<>();
        stateScore = new ArrayList<>();
        File outputFile = new File(config.output_dir, "fuzzing.log");
        log = new PrintStream(outputFile);
        prevCoverage = new ArrayList<>();
    }
    public void diffFuzz(int num, int time) throws Exception, IOException, ClassNotFoundException, InterruptedException {
        initCoverage = 100;
        messagePool = new ArrayList<>();
        messageScore = new ArrayList<>();
        Date startDate = new Date();
        String timestamp = String.valueOf(startDate.getTime() / 1000);
        start = Integer.valueOf(timestamp);
        initStandard();
        prevCoverage.add(initCoverage);
        for(int i = 1; i < diffnum; i++)
        {
            prevCoverage.add(100);
        }
        int exec_num = 0;
        seedPool = new ArrayList<>();
        seedScore = new ArrayList<>();
        readFromFile(seedPool ,seedScore, this.seed_dir);
        dictFromFile("dict");
        int seedNum = seedPool.size();
        boolean isCrash = false;
        boolean isCoverageAdded = false;
        int j = 0;
        for(int stateIndex = statePool.size()-1; stateIndex > -1; stateIndex--){
            System.out.println("Fuzzing index: " + Integer.toString(stateIndex));
            FuzzingPath curPath = statePool.get(stateIndex);
            byte[] message;
            for(int mutateIndex = 0; mutateIndex < curPath.messages.size(); mutateIndex++) {
                List<byte[]> currentSeed = deepCopy(curPath.messages);
                for(j = 0; j < currentSeed.get(mutateIndex).length; j++) {
                    HashSet<ArrayList<String>> output = new HashSet<>();
                    List<byte[]> mutateSeed = new ArrayList<>();
                    isCrash = false;
                    isCoverageAdded = false;
                    int depth = 0;
                    for (int sutIndex = 0; sutIndex < diffnum; sutIndex++){
                        sut.get(sutIndex).pre();
                        if(sutIndex==0) {
                            message = null;
                            ArrayList<String> tmp = new ArrayList<>();
                            for (int m = 0; m < currentSeed.size(); m++) {
                                if (m == mutateIndex) {
                                    //mutate the mth package
                                    message = new byte[currentSeed.get(m).length];
                                    System.arraycopy(currentSeed.get(m), 0, message, 0, currentSeed.get(m).length);
                                    message[j] ^= 0xFF;
                                    tmp.add(sut.get(sutIndex).SendFuzzingMessage(message));
                                    mutateSeed.add(message);
                                } else {
                                    tmp.add(sut.get(sutIndex).SendFuzzingMessage(currentSeed.get(m)));
                                    mutateSeed.add(currentSeed.get(m));
                                }
                                depth++;
                                if (tmp.toString().contains("Closed"))
                                    break;
                            }
                            output.add(tmp);
                            total_num++;
                        }
                        else{
                            ArrayList<String> tmp = new ArrayList<>();
                            for (byte [] m: mutateSeed) {
                                tmp.add(sut.get(sutIndex).SendFuzzingMessage(m));
                            }
                            output.add(tmp);
                        }
                        sut.get(sutIndex).post();
                        isCoverageAdded |= calculateScore(sutIndex,mutateSeed, mutateIndex,1);
                    }
                    curPath.fuzzs++;
                    if(isCoverageAdded) {
                        curPath.coverageAdd++;
                        curPath.score++;
                        stateScore.set(stateIndex, curPath.score + curPath.coverageAdd * (Math.ceil((end - start) / 600.0)) - curPath.fuzzs / 100.0);
                        curPath.addMessages(mutateSeed, 1.0 * (mutateIndex + 1) * Math.ceil((end - start) / 600.0) * 1);
                    }
                    if(output.size() > 1)
                    {
                        if(uniqueDiff.add(output.toString()) && !isCrash )
                        {
                            Date endDate = new Date();
                            String endstamp = String.valueOf(endDate.getTime() / 1000);
                            int end = Integer.valueOf(endstamp);
                            System.out.println(output);
                            seedSave(mutateSeed, this.Seed + File.separator + "Diff" + String.format("%06d", total_num) + "-"+output.toString().hashCode());
                            seedPool.add(mutateSeed);
                            seedScore.add(Math.ceil((end-start)/600.0)*depth);
                        }
                    }
                }
                /****************
                 * RANDOM HAVOC *
                 ****************/
                byte[] original = new byte[currentSeed.get(mutateIndex).length];
                System.arraycopy(currentSeed.get(mutateIndex), 0, original, 0, currentSeed.get(mutateIndex).length);
                for(int splice_index = 0;splice_index < 10; splice_index++) {
                    List<byte[]> mutateSeed = new ArrayList<>();
                    HashSet<ArrayList<String>> output = new HashSet<>();
                    isCoverageAdded = false;
                    message = new byte[original.length];
                    int fail = 0;
                    while (true) {
                        int messageIndex = getRandomSeed(messageScore);
                        byte[] message1 = new byte[messagePool.get(messageIndex).length];
                        System.arraycopy(messagePool.get(messageIndex), 0, message1, 0, messagePool.get(messageIndex).length);
                        System.arraycopy(original, 0, message, 0, original.length);
                        if (message.length < 5 || message1.length < 5)
                            break;
                        if (splice(message, message1,rand)) {
                            message = new byte[message1.length];
                            System.arraycopy(message1, 0, message, 0, message1.length);
                            break;
                        }
                        fail++;
                        if(fail>2)
                            break;
                    }
                    currentSeed.set(mutateIndex, message);
                    for (int sutIndex = 0; sutIndex < diffnum; sutIndex++) {
                        int depth = 0;
                        this.sut.get(sutIndex).pre();
                        if(sutIndex == 0) {
                            ArrayList<String> tmp = new ArrayList<>();
                            for (int m = 0; m < currentSeed.size(); m++) {
                                if (m == mutateIndex) {
                                    //mutate the mth package
                                    int mutateNum = 5;
                                    message = new byte[currentSeed.get(m).length];
                                    System.arraycopy(currentSeed.get(m), 0, message, 0, currentSeed.get(m).length);
                                    for (int i = 0; i < mutateNum; i++) {
                                        int mutateStrategy = rand.nextInt(11);
                                        if (mutateStrategy < 5) {
                                            MutateMessage mutate = new MutateMessage(rand);
                                            while (mutate.newSize == 0)
                                                mutate.MutateDispatcher(message, message.length);
                                            message = new byte[mutate.newSize];
                                            System.arraycopy(mutate.newMessage, 0, message, 0, mutate.newSize);
                                        } else if (mutateStrategy == 5) {
                                            byte[] message1 = currentSeed.get(rand.nextInt(currentSeed.size()));
                                            byte[] message2 = new byte[message.length];
                                            System.arraycopy(message, 0, message2, 0, message.length);
                                            message = new byte[message2.length + message1.length];
                                            System.arraycopy(message1, 0, message, 0, message1.length);
                                            System.arraycopy(message2, 0, message, message1.length, message2.length);
                                        } else if (mutateStrategy == 6) {
                                            byte[] message1 = currentSeed.get(rand.nextInt(currentSeed.size()));
                                            byte[] message2 = new byte[message.length];
                                            System.arraycopy(message, 0, message2, 0, message.length);
                                            message = new byte[message2.length + message1.length];
                                            System.arraycopy(message2, 0, message, 0, message2.length);
                                            System.arraycopy(message1, 0, message, message2.length, message1.length);
                                        } else if (mutateStrategy == 7 && !messagePool.isEmpty()) {
                                            int messageIndex = getRandomSeed(messageScore);
                                            byte[] message1 = messagePool.get(messageIndex);
                                            message = new byte[message1.length];
                                            System.arraycopy(message1, 0, message, 0, message1.length);
                                        } else if (mutateStrategy == 8 && !messagePool.isEmpty()) {
                                            int messageIndex = getRandomSeed(messageScore);
                                            byte[] message1 = messagePool.get(messageIndex);
                                            byte[] message2 = new byte[message.length];
                                            System.arraycopy(message, 0, message2, 0, message.length);
                                            message = new byte[message2.length + message1.length];
                                            System.arraycopy(message2, 0, message, 0, message2.length);
                                            System.arraycopy(message1, 0, message, message2.length, message1.length);
                                        } else if (mutateStrategy == 9 && !messagePool.isEmpty()) {
                                            byte[] message2 = new byte[message.length];
                                            System.arraycopy(message, 0, message2, 0, message.length);
                                            message = new byte[2 * message2.length];
                                            System.arraycopy(message2, 0, message, 0, message2.length);
                                            System.arraycopy(message2, 0, message, message2.length, message2.length);
                                        } else if (mutateStrategy == 10 && dict.size() > 0) {
                                            byte[] message1 = dict.get(rand.nextInt(dict.size()));
                                            byte[] message2 = new byte[message.length];
                                            System.arraycopy(message, 0, message2, 0, message.length);
                                            message = new byte[message2.length + message1.length];
                                            int insert_at = rand.nextInt(message2.length);
                                            System.arraycopy(message2, 0, message, 0, insert_at);
                                            System.arraycopy(message1, 0, message, insert_at, message1.length);
                                            System.arraycopy(message2, insert_at, message, insert_at+message1.length, message2.length - insert_at);
                                        }
                                    }
                                    tmp.add(sut.get(0).SendFuzzingMessage(message));
                                    mutateSeed.add(message);
                                } else {
                                    tmp.add(sut.get(0).SendFuzzingMessage(currentSeed.get(m)));
                                    mutateSeed.add(currentSeed.get(m));
                                }
                                if (tmp.toString().contains("Closed"))
                                    break;
                            }
                            output.add(tmp);
                        }
                        else
                        {
                            ArrayList<String> tmp = new ArrayList<>();
                            for (byte [] m: mutateSeed) {
                                tmp.add(sut.get(sutIndex).SendFuzzingMessage(m));
                            }
                            output.add(tmp);
                        }
                        this.sut.get(sutIndex).post();
                        isCoverageAdded |= calculateScore(sutIndex,mutateSeed, mutateIndex, 1);
                        if(output.size() > 1)
                        {
                            if(uniqueDiff.add(output.toString()) && !isCrash)
                            {
                                System.out.println(output);
                                seedSave(mutateSeed, this.Seed + File.separator + "Diff" + String.format("%06d", total_num) + "-"+output.toString().hashCode());
                                stateScore.set(stateIndex,Math.max(stateScore.get(stateIndex)+(Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1-curPath.fuzzs/100.0,10.0));
                                curPath.addMessages(mutateSeed,1.0 * (Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1);
                            }
                        }
                    }
                }
                /************
                 * SPLICING *
                 ************/
                for(j = 0; j < havoc_num; j++) {
                    List<byte[]> mutateSeed = new ArrayList<>();
                    HashSet<ArrayList<String>> output = new HashSet<>();
                    isCoverageAdded = false;
                    int depth = 0;
                    for (int sutIndex = 0; sutIndex < diffnum; sutIndex++) {
                        this.sut.get(sutIndex).pre();
                        if (sutIndex == 0) {
                            ArrayList<String> tmp = new ArrayList<>();
                            int m = 0;
                            message = null;
                            for (; m < currentSeed.size(); m++) {
                                if (m == mutateIndex) {
                                    //mutate the mth package
                                    message = new byte[currentSeed.get(m).length];
                                    while(true){
                                        int messageIndex = getRandomSeed(messageScore);
                                        byte[] message1 = new byte[messagePool.get(messageIndex).length];
                                        System.arraycopy(messagePool.get(messageIndex), 0, message1, 0, messagePool.get(messageIndex).length);
                                        System.arraycopy(currentSeed.get(m), 0, message, 0, currentSeed.get(m).length);
                                        if (rand.nextInt(2) == 0) {
                                            if (splice(message, message1,rand))
                                            {
                                                tmp.add(sut.get(sutIndex).SendFuzzingMessage(message1));
                                                mutateSeed.add(message1);
                                                break;
                                            }
                                        } else {
                                            if (splice(message1, message,rand))
                                            {
                                                tmp.add(sut.get(sutIndex).SendFuzzingMessage(message));
                                                mutateSeed.add(message);
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    tmp.add(sut.get(sutIndex).SendFuzzingMessage(currentSeed.get(m)));
                                    mutateSeed.add(currentSeed.get(m));
                                }
                                depth++;
                                if (tmp.toString().contains("Closed"))
                                    break;
                            }
                            total_num++;
                            curPath.fuzzs++;
                        }
                        else
                        {
                            ArrayList<String> tmp = new ArrayList<>();
                            for (byte [] m: mutateSeed) {
                                tmp.add(sut.get(sutIndex).SendFuzzingMessage(m));
                            }
                            output.add(tmp);
                        }
                        this.sut.get(sutIndex).post();
                        isCoverageAdded |= calculateScore(sutIndex,mutateSeed, mutateIndex, 1);
                    }
                    curPath.fuzzs++;
                    if(isCoverageAdded) {
                        curPath.coverageAdd++;
                        curPath.score++;
                        stateScore.set(stateIndex, curPath.score + curPath.coverageAdd * (Math.ceil((end - start) / 600.0)) - curPath.fuzzs / 100.0);
                        curPath.addMessages(mutateSeed, 1.0 * (mutateIndex + 1) * Math.ceil((end - start) / 600.0) * 1);
                    }
                    if(output.size() > 1)
                    {
                        if(uniqueDiff.add(output.toString()) )
                        {
                            System.out.println(output);
                            seedSave(mutateSeed, this.Seed + File.separator + "Diff" + String.format("%06d", total_num) + "-"+output.toString().hashCode());
                            stateScore.set(stateIndex,Math.max(stateScore.get(stateIndex)+(Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1-curPath.fuzzs/100.0,10.0));
                            curPath.addMessages(mutateSeed,1.0 * (Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1);
                        }
                    }
                }
            }
        }
        System.out.println("Start random");
        while(true) {
            int stateIndex = getRandomSeed(stateScore);
            FuzzingPath curPath = statePool.get(stateIndex);
            int loop = 0;
            while (curPath.messagesList.isEmpty()) {
                stateIndex = getRandomSeed(stateScore);
                curPath = statePool.get(stateIndex);
            }
            while (true) {
                loop++;
                if (loop > 20)
                    break;
                HashSet<ArrayList<String>> output = new HashSet<>();
                int seedIndex = getRandomSeed(curPath.messagesScore);
                List<byte[]> currentSeed = deepCopy(curPath.messagesList.get(seedIndex));
                if (rand.nextInt(2) == 0) {
                    List<byte[]> currentSeed2 = deepCopy(seedPool.get(getRandomSeed(seedScore)));
                    currentSeed = currentSeed.subList(0, rand.nextInt(currentSeed.size()));
                    currentSeed2 = currentSeed2.subList(rand.nextInt(currentSeed2.size()), currentSeed2.size());
                    currentSeed.addAll(currentSeed2);
                }
                if (rand.nextInt(3) == 2)
                    Collections.shuffle(currentSeed);
                int mutateIndex = rand.nextInt(currentSeed.size());
                List<byte[]> mutateSeed = new ArrayList<>();
                isCrash = false;
                for (int sutIndex = 0; sutIndex < diffnum; sutIndex++) {
                    this.sut.get(sutIndex).pre();
                    if(sutIndex==0) {
                        for (int m = 0; m < currentSeed.size(); m++) {
                            ArrayList<String> tmp = new ArrayList<>();
                            byte[] message = new byte[currentSeed.get(m).length];
                            System.arraycopy(currentSeed.get(m), 0, message, 0, currentSeed.get(m).length);
                            if (m == mutateIndex) {
                                int mutateNum = 5;
                                for (int i = 0; i < mutateNum; i++) {
                                    int mutateStrategy = rand.nextInt(12);
                                    if (mutateStrategy < 5) {
                                        MutateMessage mutate = new MutateMessage(rand);
                                        while (mutate.newSize == 0)
                                            mutate.MutateDispatcher(message, message.length);
                                        message = new byte[mutate.newSize];
                                        System.arraycopy(mutate.newMessage, 0, message, 0, mutate.newSize);
                                    } else if (mutateStrategy == 5) {
                                        byte[] message1 = currentSeed.get(rand.nextInt(currentSeed.size()));
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        message = new byte[message2.length + message1.length];
                                        System.arraycopy(message1, 0, message, 0, message1.length);
                                        System.arraycopy(message2, 0, message, message1.length, message2.length);
                                    } else if (mutateStrategy == 6) {
                                        byte[] message1 = currentSeed.get(rand.nextInt(currentSeed.size()));
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        message = new byte[message2.length + message1.length];
                                        System.arraycopy(message2, 0, message, 0, message2.length);
                                        System.arraycopy(message1, 0, message, message2.length, message1.length);
                                    } else if (mutateStrategy == 7 && !messagePool.isEmpty()) {
                                        int messageIndex = getRandomSeed(messageScore);
                                        byte[] message1 = messagePool.get(messageIndex);
                                        message = new byte[message1.length];
                                        System.arraycopy(message1, 0, message, 0, message1.length);
                                    } else if (mutateStrategy == 8 && !messagePool.isEmpty()) {
                                        int messageIndex = getRandomSeed(messageScore);
                                        byte[] message1 = messagePool.get(messageIndex);
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        message = new byte[message2.length + message1.length];
                                        System.arraycopy(message2, 0, message, 0, message2.length);
                                        System.arraycopy(message1, 0, message, message2.length, message1.length);
                                    } else if (mutateStrategy == 9 && !messagePool.isEmpty()) {
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        message = new byte[2 * message2.length];
                                        System.arraycopy(message2, 0, message, 0, message2.length);
                                        System.arraycopy(message2, 0, message, message2.length, message2.length);
                                    } else if (mutateStrategy == 10 && dict.size() > 0) {
                                        byte[] message1 = dict.get(rand.nextInt(dict.size()));
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        message = new byte[message2.length + message1.length];
                                        int insert_at = rand.nextInt(message2.length);
                                        System.arraycopy(message2, 0, message, 0, insert_at);
                                        System.arraycopy(message1, 0, message, insert_at, message1.length);
                                        System.arraycopy(message2, insert_at, message, insert_at + message1.length, message2.length - insert_at);
                                    } else {
                                        byte[] message2 = new byte[message.length];
                                        System.arraycopy(message, 0, message2, 0, message.length);
                                        int fail = 0;
                                        while (true) {
                                            int messageIndex = getRandomSeed(messageScore);
                                            byte[] message1 = new byte[messagePool.get(messageIndex).length];
                                            System.arraycopy(messagePool.get(messageIndex), 0, message1, 0, messagePool.get(messageIndex).length);
                                            System.arraycopy(message2, 0, message, 0, message2.length);
                                            if (message.length < 5 || message1.length < 5)
                                                break;
                                            if (splice(message2, message1,rand)) {
                                                message = new byte[message1.length];
                                                System.arraycopy(message1, 0, message, 0, message1.length);
                                                break;
                                            }
                                            fail++;
                                            if (fail > 2)
                                                break;
                                        }
                                    }
                                }
                            }
                            tmp.add(sut.get(sutIndex).SendFuzzingMessage(message));
                            mutateSeed.add(message);
                            if (tmp.toString().contains("Closed"))
                                break;
                        }
                        total_num++;
                    }
                    else
                    {
                        ArrayList<String> tmp = new ArrayList<>();
                        for (byte [] m: mutateSeed) {
                            tmp.add(sut.get(sutIndex).SendFuzzingMessage(m));
                        }
                        output.add(tmp);
                    }
                    this.sut.get(sutIndex).post();
                    isCoverageAdded |= calculateScore(sutIndex,mutateSeed, mutateIndex, 1);

                }
                curPath.fuzzs++;
                if(isCoverageAdded) {
                    curPath.coverageAdd++;
                    curPath.score++;
                    stateScore.set(stateIndex, curPath.score + curPath.coverageAdd * (Math.ceil((end - start) / 600.0)) - curPath.fuzzs / 100.0);
                    curPath.addMessages(mutateSeed, 1.0 * (mutateIndex + 1) * Math.ceil((end - start) / 600.0) * 1);
                }
                if(output.size() > 1)
                {
                    if(uniqueDiff.add(output.toString()) )
                    {
                        System.out.println(output);
                        seedSave(mutateSeed, this.Seed + File.separator + "Diff" + String.format("%06d", total_num) + "-"+output.toString().hashCode());
                        stateScore.set(stateIndex,Math.max(stateScore.get(stateIndex)+(Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1-curPath.fuzzs/100.0,10.0));
                        curPath.addMessages(mutateSeed,1.0 * (Math.min(mutateIndex, mutateSeed.size() - 1)+1)*Math.ceil((end-start)/600.0) * 1);
                    }
                }
                Date endDate = new Date();
                String endstamp = String.valueOf(endDate.getTime() / 1000);
                end = Integer.valueOf(endstamp);
                if (end - start > time)
                    break;
            }
            if (end - start > time)
                break;
        }
        log.println("The execs done : " + total_num);
        log.close();
        for(int sutIndex = 0 ; sutIndex < sut.size(); sutIndex++){
            sut.get(sutIndex).exit();
        }

    }

    public static void main(String[] args) throws Exception {
        if(args.length < 1) {
            System.err.println("Invalid number of parameters");
            System.exit(-1);
        }
        int totalTime = Integer.parseInt(args[1]);
        LearningConfig config = new LearningConfig(args[0]);
        FuzzingBasedDiff fuzzingBasedDiff =
                new FuzzingBasedDiff(config,"learnedModel.dot");
        // define the fuzzing number on every state
        fuzzingBasedDiff.diffFuzz(50, totalTime);
    }

    public void readFromFile(ArrayList<List<byte[]>> seedPool, ArrayList<Double> seedScore,String dir)
    {
        List<String> files = new ArrayList<String>();
        File file = new File(dir);
        if(file.length()==0)
            return;
        File[] tempList = file.listFiles();
        List fileList = Arrays.asList(tempList);
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                if (o1.isDirectory() && o2.isFile())
                    return -1;
                if (o1.isFile() && o2.isDirectory())
                    return 1;
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (int i = 0; i < tempList.length; i++) {
            if (tempList[i].isFile()) {
                files.add(tempList[i].toString());
                File temp = new File(tempList[i].toString());
                Double score = 10*1.0;
                try {
                    score = Double.parseDouble(tempList[i].toString().substring(tempList[i].toString().length() - 5, tempList[i].toString().length()));
                }
                catch (Exception e)
                {
                    score = 10*1.0;
                }
                try {
                    InputStream fileIn = new FileInputStream(temp);
                    //DataInputStream in = new DataInputStream(fileIn);

                    // 使用缓存区读入对象效率更快
                    BufferedInputStream in = new BufferedInputStream(fileIn);
                    byte[] len = new byte[1024] ;
                    int length;
                    List<byte[]> seed = new ArrayList<>();
                    Word<String> inputList = Word.epsilon();
                    List<MealyState> initList = new LinkedList<MealyState>();
                    initList.add(initialState);
                    while(in.read(len,0,4)!=-1)
                    {
                        length = byteArrayToInt(len);
                        byte[] bys = new byte[length];
                        in.read(bys,0,length);
                        seed.add(bys);
                        inputList.append(sut.get(0).messageToSymbol(bys));
                        messagePool.add(bys);
                        messageScore.add(score);
                    }
                    seedPool.add(seed);
                    seedScore.add(10.0);
                    FuzzingPath tmp = new FuzzingPath(initList,seed,inputList);
                    System.out.println(inputList);
                    statePool.add(tmp);
                    stateScore.add(0.0);
                    in.close();
                    fileIn.close();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public void dictFromFile(String dictFile)
    {
        Double score = 10*1.0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(dictFile));
            String keyword;
            int length;
            while((keyword=br.readLine())!=null)
            {
                dict.add(keyword.getBytes("US-ASCII"));
            }
            br.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public void initStandard() throws IOException, ClassNotFoundException, InterruptedException
    {
        Queue<TransitionRecord> bfsQueue = new ArrayDeque<>();
        List<MealyState> initList = new LinkedList<MealyState>();
        initList.add(initialState);
        bfsQueue.add(new TransitionRecord(initialState,Word.epsilon(),Word.epsilon(), new LinkedList<byte[]>(),initList));
        TransitionRecord curr = bfsQueue.poll();
        int j = 0;

        String sinkedStateName="";
        if(sinkedState!=null)
            sinkedStateName = sinkedState.getName();
        while(curr != null)
        {
            MealyState currState = curr.state;
            for(int i = 0; i < currState.getInputs().size(); i++)
            {
                this.sut.get(0).pre();
                ArrayList<byte[]> testCase = new ArrayList<>();
                List<byte[]> messageList = deepCopy(curr.messageList);
                List<MealyState> stateList = deepCopy(curr.stateList);
                String input = currState.getInputs().get(i);
                OutputTransition outputTransition = currState.getOutputTransition(input);
                MealyState targetState = outputTransition.state;
                if(targetState != null ){
                    Iterator iter = curr.accessSequence.iterator();
                    while (iter.hasNext())
                    {
                        String tmpInput = iter.next().toString();
                        sut.get(0).step(tmpInput);
                    }
                    String output = sut.get(0).step(input);
                    sut.get(0).post();
                    if((stateContain(stateList,targetState) || targetState.getName().equals(sinkedStateName)) && !output.contains("ConnectionClosed"))
                    {
                        byte[] message = sut.get(0).getCurrent();
                        if(message != null)
                            messageList.add(message);
                        stateList.add(targetState);
                        Word<String> inputList = curr.accessSequence.append(input);
                        Word<String> outputList = curr.outputSequence.append(output);
                        FuzzingPath tmp = new FuzzingPath(stateList,messageList,inputList);
                        System.out.println(inputList);
                        statePool.add(tmp);
                        stateScore.add(0.0);
                        total_num++;
                        j++;
                    }
                    else if(!output.contains("ConnectionClosed"))
                    {
                        byte[] message = sut.get(0).getCurrent();
                        if(message != null)
                            messageList.add(message);
                        stateList.add(targetState);
                        Word<String> inputList = curr.accessSequence.append(input);
                        Word<String> outputList = curr.outputSequence.append(output);
                        TransitionRecord newRecord = new TransitionRecord(targetState,inputList,outputList,messageList,stateList);
                        total_num++;
                        j++;
                        bfsQueue.add(newRecord);
                    }
                }
            }
            curr = bfsQueue.poll();
        }
        bfsQueue.clear();
        bfsQueue = null;
    }

    public boolean stateContain(List<MealyState> stateList, MealyState targetState)
    {
        for (MealyState tmp : stateList ) {
            if(tmp.getName().equals(targetState.getName()))
                return true;
        }
        return false;
    }

    public boolean calculateScore(int sutIndex, List<byte[]> mutateSeed, int mutateIndex, int weight) throws IOException{
        Date endDate = new Date();
        String endstamp = String.valueOf(endDate.getTime() / 1000);
        end = Integer.valueOf(endstamp);
        int coverage = this.sut.get(sutIndex).getCoverage();
        if (coverage == 1) {
            System.out.println("Crash");
            seedSave(mutateSeed, this.Crash  + File.separator + "Crash_" + String.format("%06d", total_num) + "_" + String.format("%05d", coverage));
            seedSave(mutateSeed, this.Seed + File.separator + "Fuzzing" + String.format("%06d", total_num) + "-" + String.format("%05d", coverage));
        }
        if (coverage > prevCoverage.get(sutIndex)) {
            if (coverage == 0)
                coverage = initCoverage;
            System.out.println(coverage);
            if (coverage > prevCoverage.get(sutIndex)) {
                seedSave(mutateSeed, this.Seed + File.separator + "Fuzzing" + String.format("%06d", total_num) + "-" + String.format("%05d", coverage));
                seedPool.add(mutateSeed);
                seedScore.add(1.0 * (mutateIndex+1)*Math.ceil((end-start)/600.0) * weight);
                messagePool.add(mutateSeed.get(Math.min(mutateIndex,mutateSeed.size()-1)));
                messageScore.add(1.0 * (mutateIndex+1)*Math.ceil((end-start)/600.0)  * weight);
            }
            prevCoverage.set(sutIndex, coverage);
            return true;
        }
        return false;
    }

}
