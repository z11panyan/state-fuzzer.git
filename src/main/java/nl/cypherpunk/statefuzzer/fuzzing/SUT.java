package nl.cypherpunk.statefuzzer.fuzzing;

import de.learnlib.api.SUL;

import java.io.IOException;

public interface SUT<I,O> extends SUL<I, O> {
    public String SendFuzzingMessage(byte[] message) throws Exception;

    public String messageToSymbol(byte[] message);

    public int getCoverage() throws IOException;

    public byte[] getCurrent();

    public void exit();
}
